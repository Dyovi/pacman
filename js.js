//Tablero de 30x30
var tablero = [
//   0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  //0
    [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],  //1
    [0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0],  //2
    [0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0],  //3
    [0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0],  //4
    [0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0],  //5
    [0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0],  //6
    [0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0],  //7
    [0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0],  //8
    [0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0],  //9
    [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0],  //10
    [0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0],  //11
    [0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0],  //12
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],  //13
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],  //14
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],  //15
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],  //16
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],  //17
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],  //18
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],  //19
    [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],  //20
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],  //21
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],  //22
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],  //23
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],  //24
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],  //25
    [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],  //26
    [0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0],  //27
    [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0],  //28
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]   //29
]

//Varios
var fantasmas = []
var jugador = {}
var juegoPerdido = false
var juegoGanado = false
var minutosParaFinPartida = 5
var alturaZonaPuntuacion = 30
var ratioSpawnFantasma = 20

// Colores 
var colorMuros = "darkgreen"
var colorSuelo = "white"
var colorJugador = "coral"
var colorFantasmas = "deeppink"

//Puntuación
var puntuacion = 0
var mejorPuntuacion = 0
if (document.cookie.includes("mejorPuntuacion")){
    var valor = document.cookie.split("=")[1]
    mejorPuntuacion = valor
} else {
    document.cookie = `mejorPuntuacion=${mejorPuntuacion}`
}

//Iinicializo las posiciones de los fantasmas y jugador iniciales
iniPosiciones()
//dibujarConsola()
//Dibuja el canvas del mapa y los personajes en sus posiciones iniciales
dibujarCanvas()

//Cada 600 ms ejecuta lo siguiente
var intervalo = setInterval(() => {
    cambiarDireccion() //Comprobaciones para cambiar o no de dirección
    mover() //Mover los personajes
    //dibujarConsola()
    dibujarCanvas() //Dibujar el tablero y los personajes
}, 600)

//Cada 1000 ms ejecuta lo siguiente
var intervaloPuntuacion = setInterval(() => {
    subirPuntuacion() //Sube la puntuación cada segundo.
    //comprobarJuegoGanado() //Comprobar si se ha ganado la partida.
    spawnFantasma() //Crea nuevo fantasma tras pasar X segundos
}, 1000)

//Spawnea un nuevo fantasma tras el primer minuto de juego cada 20 segundos (empezando en el segundo 80)
function spawnFantasma(){

    if (puntuacion > 60 && puntuacion%ratioSpawnFantasma == 0){
        var x = 0
        var y = 0
    
        do {
            x = Math.floor(Math.random()*30)
            y = Math.floor(Math.random()*30)
        } while (tablero[y][x] != 1);
    
        var direccion = direccionRandom(y,x)
        tablero[y][x] = "F"
        fantasmas.push({x: x, y: y, d: direccion, tipo: 'F'})
    }
    
}

function iniPosiciones(){
    //Bucle de 4 veces para escoger las posiciones de los personajes.
    for (let i = 0; i <= 3; i++) {

        var x = 0
        var y = 0
    
        do {
            x = Math.floor(Math.random()*30)
            y = Math.floor(Math.random()*30)
        } while (tablero[y][x] != 1); //Va buscando posiciones hasta dar con una donde no haya muro
    
       var direccion = direccionRandom(y,x)
        
        if (i < 3){ //fantasmas
            tablero[y][x] = "F"
            fantasmas.push({x: x, y: y, d: direccion, tipo: 'F'})
        } else { //jugador
            tablero[y][x] = "J"
            jugador = {x: x, y: y, d: direccion, tipo: 'J'}
        }
        
    }
}

//Mando a comprobar si pueden cambiar de dirección los personajes
function cambiarDireccion(){
    //comprobarCambioDeDireccion(jugador)
    fantasmas.forEach(element => {
        comprobarCambioDeDireccion(element)
    });
}

//Si el fantasma está en un cruce o tiene una pared delante, se le asigna una nueva dirección posible.
function comprobarCambioDeDireccion(element){
    if (numeroDireccionesPosibles(element.y, element.x) > 2 || hayParedDelante(element)){
        element.d = direccionRandom(element.y, element.x)
    }
}

//Mueve los personajes
function mover(){
    ejecutarMovimiento(jugador)
    fantasmas.forEach(element => {
        ejecutarMovimiento(element)
    });
}

//Mueve el personaje pasado a la dirección asignada
function ejecutarMovimiento(element){

    //Básicamente sería para el movimiento del jugador, en cuanto llegue contra la pared dejará de moverse hasta que el usuario cambie de dirección.
    if (hayParedDelante(element)) return

    tablero[element.y][element.x] = 1
    switch(element.d){
        case "up":
            tablero[element.y-1][element.x] = element.tipo       
            element.y = element.y-1
            break;
        case "down":
            tablero[element.y+1][element.x] = element.tipo        
            element.y = element.y+1
            break;
        case "left":
            tablero[element.y][element.x-1] = element.tipo        
            element.x = element.x-1
            break;
        case "right":
            tablero[element.y][element.x+1] = element.tipo        
            element.x = element.x+1
            break;
    }
    comprobarFinPartida() //Comprueba si han chocado fantasma y jugador.
}

//Devuelve la cantidad de direcciones posibles
function numeroDireccionesPosibles(y, x){
    var num = 0
    if (checkDireccionPosible(y-1, x)) {
        num++
    }
    if (checkDireccionPosible(y+1, x)) {
        num++
    }
    if (checkDireccionPosible(y, x-1)) {
        num++
    }
    if (checkDireccionPosible(y, x+1)) {
        num++
    }
}

//Devuelve si delante hay una pared
function hayParedDelante(element){
    switch(element.d){
        case "up":
            if (!checkDireccionPosible(element.y-1, element.x)) return true
            break;
        case "down":
            if (!checkDireccionPosible(element.y+1, element.x)) return true
            break;
        case "left":
            if (!checkDireccionPosible(element.y, element.x-1)) return true
            break;
        case "right":
            if (!checkDireccionPosible(element.y, element.x+1)) return true
            break;
    }
}

//Guarda en un array las direcciones posibles y luego devuelve de forma "aleatoria" una de las posibles direcciones
function direccionRandom(y, x){
    var direcciones = []
    if (checkDireccionPosible(y-1, x)) direcciones.push("up") 
    if (checkDireccionPosible(y+1, x)) direcciones.push("down") 
    if (checkDireccionPosible(y, x-1)) direcciones.push("left") 
    if (checkDireccionPosible(y, x+1)) direcciones.push("right") 
    return direcciones[Math.floor(Math.random()*direcciones.length)]
}

//Comprueba si la dirección pasada es un muro. En caso de serlo, no es una casilla donde un personaje pueda estar.
function checkDireccionPosible(y, x){
    if (tablero[y][x] == 0) return false
    return true
}

//Recorre las posiciones de los fantasmas y las compara con la del jugador. Si coinciden, están en la misma casilla, por lo que se acaba la partida.
function comprobarFinPartida(){
    fantasmas.forEach(element => {
        if (jugador.x == element.x && jugador.y == element.y){
            juegoPerdido = true
        }
    });
}

//Recoge las teclas pulsadas por el usuario, y las filtra, quedándose solo con las flechas de dirección.
//Si el movimiento dicho por el usuario es posible, se le asignará la dirección en cuestión al personaje del jugador.
function teclaMovimiento(e){
    switch(e.keyCode){
        case 40: //abajo
            if (checkDireccionPosible(jugador.y+1, jugador.x))
                jugador.d = "down"
            break
        case 38: //arriba
            if (checkDireccionPosible(jugador.y-1, jugador.x))
                jugador.d = "up"
            break
        case 37: //izquierda
            if (checkDireccionPosible(jugador.y, jugador.x-1))
                jugador.d = "left"
            break
        case 39: //derecha
            if (checkDireccionPosible(jugador.y, jugador.x+1))
                jugador.d = "right"
            break
    }
}

//Aumenta la puntuación actual. Si la puntuación es mayor a la mejor puntuación obtenida, se actualiza esta última.
function subirPuntuacion(){
    if (!juegoPerdido && !juegoGanado){
        puntuacion++;
        if (puntuacion > mejorPuntuacion){
            mejorPuntuacion = puntuacion
            document.cookie = `mejorPuntuacion=${mejorPuntuacion}`
        }
    }
}

//Comprueba si la partida se ha terminado por pasar X minutos sin ser comido.
function comprobarJuegoGanado(){
    if (puntuacion >= minutosParaFinPartida*60){ //Mayor o igual por si se le fuese la olla y subiese de más la puntuación.
        juegoGanado = true
    }
}

//Dibuja un canvas del tablero de juego.
function dibujarCanvas(){
    //Se obtiene la referencia del canvas
    var c = document.getElementById("canvas")
    var ctx = c.getContext("2d")
    
    //Se limpia para evitar que se pegue encima del anterior (si no, los textos se van haciendo más gruesos por ejemplo)
    ctx.clearRect(0, 0, canvas.width, canvas.height)

    //Muestra la puntuación actual
    ctx.font = "15px Arial"
    ctx.fillStyle = "#111"
    ctx.textAlign = 'left'
    ctx.fillText(`Puntuación: ${puntuacion}`, 5, 15)

    //Muestra la mejor puntuación
    ctx.font = "15px Arial"
    ctx.fillStyle = "#111"
    ctx.textAlign = 'right'
    ctx.fillText(`Mejor puntuación: ${mejorPuntuacion}`, 295, 15) 

    tablero.forEach((element, y) => { //Recorre las filas del tablero

        element.forEach((tipo, x) => { //Recorre las columnas de cada fila
    
            var color = ""

            switch (tipo){
                //Dependiendo del tipo de casilla (si es muro, suelo, jugador o fantasma) asigna un color u otro.
                case 0:
                    color = colorMuros
                    break
                case 'F':
                    color = colorFantasmas
                    break
                case 'J':
                    color = colorJugador
                    break
                default:
                    color = colorSuelo
                    break
            }

            //Dibuja la casilla con el color pertinente
            ctx.fillStyle = color
            //Se multiplica la x e y por el tamaño de cada casilla, para que esté colocada en la posición correcta.
            ctx.fillRect(x*10, y*10+alturaZonaPuntuacion, 10, 10)
            ctx.stroke()

        });
    });

    var msg = ""

    //Comprobamos si la partida ha sido perdida o ganada, dibujando en el canvas un cartel indicándolo de ser el caso.
    if (juegoPerdido) msg = "GAME OVER"
    else if (juegoGanado) msg = "VICTORIA"

    if (juegoPerdido || juegoGanado){
        clearInterval(intervalo)
        clearInterval(intervaloPuntuacion)
        ctx.font = "40px Arial"
        ctx.fillStyle = '#111'
        ctx.textAlign = 'center'
        ctx.fillText(msg, 150, 150)
    }
    
}

//Dibuja en consola el tablero de juego.
function dibujarConsola(){

    var board = ""

    fantasmas.forEach((fantasma, i) => {
        board += `Fantasma ${i+1} [${fantasma.y},${fantasma.x}] se está moviendo dirección ${fantasma.d}\n`
    });
    
    board += `Jugador [${jugador.y},${jugador.x}] se quiere mover dirección ${jugador.d}\n\n`
    
    tablero.forEach((element, y) => {
        var fila = ""
    
        element.forEach((tipo, x) => {

            switch (tipo){
                case 0:
                    fila += "X "
                    break
                case 'F':
                    fila += "¶ "
                    break
                case 'J':
                    fila += "@ "
                    break
                default:
                    fila += "  "
                    break
            }

        });
        board += fila+"\n"
    });

    if (juegoPerdido){
        clearInterval(intervalo)
        clearInterval(intervaloPuntuacion)
        board += "\n### GAME OVER ###"
    }

    if (juegoGanado){
        clearInterval(intervalo)
        clearInterval(intervaloPuntuacion)
        board += "\n### WIN ###"
    }
    
    console.log(board)
}